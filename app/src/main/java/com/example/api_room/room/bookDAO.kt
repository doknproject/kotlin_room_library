package com.example.api_room.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.api_room.room.BookEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface BookDAO {

    @Insert
    suspend fun addBook( bookEntity : BookEntity)

    @Query("SELECT * FROM BookEntity")
    fun getAllBooks(): Flow<List<BookEntity>>

    @Delete
    suspend fun deleteBook(bookEntity : BookEntity)

}