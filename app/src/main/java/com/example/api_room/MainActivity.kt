package com.example.api_room

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModel
import com.example.api_room.repository.Repository
import com.example.api_room.room.BookEntity
import com.example.api_room.room.BooksDB
import com.example.api_room.ui.theme.ApiroomTheme
import com.example.api_room.viewmodel.BookViewModel

class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            ApiroomTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) {

                    val mContext = LocalContext.current
                    val db = BooksDB.getInstance(mContext)
                    val repository = Repository(db)
                    val myViewModel = BookViewModel(repository = repository)
                    MainScreen(myViewModel)
                    

                }
            }
        }
    }
}

@Composable
fun MainScreen(viewModel: BookViewModel) {

    var inputBook by remember {
        mutableStateOf("")
    }
    Column( horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(start = 30.dp, top = 50.dp)
            .systemBarsPadding()) {
        TextField(
            value = inputBook,
            onValueChange = {
                            enteredText -> inputBook = enteredText
            },
            label = {  Text("Book Name!") },
            placeholder =  { Text(text = "  Enter Your Book Name ") }
        )
        Button(onClick = { viewModel.addbook(BookEntity(0,inputBook)) }) {

            Text(text = "Insert Book into DB")
            
        }
        BooksList(viewModel = viewModel)
        
    }
   
}

@Composable
fun BookCard(viewModel: BookViewModel, book: BookEntity) {

    Card(modifier = Modifier
        .padding(8.dp)
        .fillMaxWidth()) {

        Row {
            Text(text = "" + book.id, fontSize = 24.sp, modifier = Modifier
                .padding(start = 4.dp, end = 4.dp))

            Text(text = book.title, fontSize = 24.sp)
            
            IconButton(onClick = { viewModel.deleteBook(book = book) }) {
                Icon(imageVector = Icons.Default.Delete, contentDescription ="" )
                
            }
        }
        
    }

}

@Composable
fun BooksList(viewModel: BookViewModel) {
    val books by viewModel.books.collectAsState(initial = emptyList())

    LazyColumn {
        items(items = books){
            item -> BookCard(
            viewModel = viewModel , book = item
        )
        }
    }
    
}