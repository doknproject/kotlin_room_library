package com.example.api_room.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.api_room.repository.Repository
import com.example.api_room.room.BookEntity
import kotlinx.coroutines.launch

class BookViewModel(val repository: Repository): ViewModel() {

    fun addbook(book:BookEntity){
        viewModelScope.launch {
            repository.addBookToRoom(book)
        }
    }
    val books = repository.getAllBooks()

    fun deleteBook(book : BookEntity){
        viewModelScope.launch {
            repository.deleteBookFromRoom(book)
        }
    }

}
