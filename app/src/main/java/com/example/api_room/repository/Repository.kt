package com.example.api_room.repository
import com.example.api_room.room.BookEntity
import com.example.api_room.room.BooksDB

class Repository(val booksDB : BooksDB) {
    suspend fun addBookToRoom(bookEntity: BookEntity){
        booksDB.bookDao().addBook(bookEntity)
    }

    fun getAllBooks() = booksDB.bookDao().getAllBooks()

    suspend fun deleteBookFromRoom(bookEntity: BookEntity){
        booksDB.bookDao().deleteBook(bookEntity = bookEntity)
    }
}